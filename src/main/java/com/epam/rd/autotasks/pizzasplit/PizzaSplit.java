package com.epam.rd.autotasks.pizzasplit;

import java.util.Scanner;

public class PizzaSplit {
    public static void main(String[] args) {
        //Write a program, reading number of people and number of pieces per pizza and then
        //printing minimum number of pizzas to order to split all the pizzas equally and with no remainder

        Scanner scanner = new Scanner(System.in);
        int numberOfPeople = scanner.nextInt();
        int numberOfPiecesPerPizza = scanner.nextInt();
        int numberOfPizzas = 0;

        for (int i = 1; i < 50; i++) {
            if ((numberOfPiecesPerPizza * i) % numberOfPeople == 0) {
                numberOfPizzas = i;
                System.out.println(numberOfPizzas);
                return;
            }
        }

    }
}
